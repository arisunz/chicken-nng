#>
#include <string.h>
#include <nng/nng.h>
#include <nng/protocol/reqrep0/rep.h>
#include <nng/protocol/reqrep0/req.h>
#include <nng/protocol/pipeline0/pull.h>
#include <nng/protocol/pipeline0/push.h>
#include <nng/protocol/pubsub0/pub.h>
#include <nng/protocol/pubsub0/sub.h>
#include <nng/protocol/pair1/pair.h>
#include <nng/protocol/survey0/survey.h>
#include <nng/protocol/survey0/respond.h>
#include <nng/protocol/bus0/bus.h>
#include <nng/transport/tls/tls.h>
#include <nng/supplemental/tls/tls.h>
<#
(module nng

    (make-rep-socket
     make-req-socket
     make-pull-socket
     make-push-socket
     make-pub-socket
     make-sub-socket
     make-pair-socket
     make-surveyor-socket
     make-respondent-socket
     make-bus-socket
     make-dialer
     make-listener
     dialer-start!
     listener-start!
     dialer-close!
     listener-close!
     socket-get socket-set!
     listener-get listener-set!
     dialer-get dialer-set!
     tls-register!
     tls-enabled?
     set-dialer-tls-certificate!
     set-listener-tls-own-certificate!
     nng-dial
     nng-listen
     nng-send
     nng-recv
     nng-close!)

  (import scheme
          (chicken base)
          (chicken condition)
          (chicken foreign)
          (chicken format)
          (chicken gc)
          foreigners
          srfi-18
          srfi-69)

  ;;;
  ;;; foreign constructors/destructors
  ;;;
  (define-foreign-record-type (nng-socket "nng_socket")
    (constructor: make-foreign-nng-socket)
    (destructor: free-foreign-nng-socket))

  (define-foreign-record-type (nng-dialer "nng_dialer")
    (constructor: make-foreign-nng-dialer)
    (destructor: free-foreign-nng-dialer))

  (define-foreign-record-type (nng-listener "nng_listener")
    (constructor: make-foreign-nng-listener)
    (destructor: free-foreign-nng-listener))

  (define nng-strerror
    (foreign-lambda c-string "nng_strerror" int))

  ;;;
  ;;; Foreign socket creation
  ;;;
  (define nng-req-open
    (foreign-lambda int "nng_req0_open" nng-socket))

  (define nng-rep-open
    (foreign-lambda int "nng_rep0_open" nng-socket))

  (define nng-push-open
    (foreign-lambda int "nng_push0_open" nng-socket))

  (define nng-pull-open
    (foreign-lambda int "nng_pull0_open" nng-socket))

  (define nng-pub-open
    (foreign-lambda int "nng_pub0_open" nng-socket))

  (define nng-sub-open
    (foreign-lambda int "nng_sub0_open" nng-socket))

  (define nng-pair-open
    (foreign-lambda int "nng_pair1_open" nng-socket))

  (define nng-surveyor-open
    (foreign-lambda int "nng_surveyor0_open" nng-socket))

  (define nng-respondent-open
    (foreign-lambda int "nng_respondent0_open" nng-socket))

  (define nng-bus-open
    (foreign-lambda int "nng_bus0_open" nng-socket))

  ;;;
  ;;; Dialer creation/configuration
  ;;;
  (define make-dialer*
    (foreign-lambda* int ((nng-dialer dialer) (nng-socket socket) (c-string url))
      "C_return(nng_dialer_create(dialer, *socket, url));"))

  (define dialer-close!*
    (foreign-lambda* int ((nng-dialer dialer))
      "C_return(nng_dialer_close(*dialer));"))

  (define (make-dialer socket url)
    (let* ((dialer (make-foreign-nng-dialer))
           (code (make-dialer* dialer socket url)))
      (if (= code 0)
          (begin
            ;; no need to close dialer on GC
            ;; since it gets closed with its socket
            (set-finalizer! dialer free-foreign-nng-dialer)
            dialer)
          (signal (condition `(exn nng dialer message
                                   ,(format "failed to create dialer - ~a~n"
                                            (nng-strerror code))))))))

  (define (dialer-close! dialer)
    (let ((code (dialer-close!* dialer)))
      (unless (= code 0)
        (signal (condition `(exn nng dialer message
                                 ,(format "failed to close dialer - ~a~n"
                                          (nng-strerror code))))))))

  (define dialer-start!*
    (foreign-lambda* int ((nng-dialer dialer) (bool non_blocking))
      "C_return(nng_dialer_start(*dialer, non_blocking ? NNG_FLAG_NONBLOCK : 0));"))

  (define (dialer-start! dialer #!optional (non-blocking? #f))
    (let ((code (dialer-start!* dialer non-blocking?)))
      (unless (= code 0)
        (signal (condition `(exn nng dialer message
                                 ,(format "failed to start dialer - ~a~n"
                                          (nng-strerror code))))))))

  (define dialer-set-ptr!
    (foreign-lambda* int ((nng-dialer dialer) (c-string opt) (c-pointer ptr))
      "C_return(nng_dialer_set_ptr(*dialer, opt, ptr));"))

  ;;;
  ;;; Listener stuff
  ;;;
  (define make-listener*
    (foreign-lambda* int ((nng-listener listener) (nng-socket socket) (c-string url))
      "C_return(nng_listener_create(listener, *socket, url));"))

  (define listener-close!*
    (foreign-lambda* int ((nng-listener listener))
      "C_return(nng_listener_close(*listener));"))

  (define (make-listener socket url)
    (let* ((listener (make-foreign-nng-listener))
           (code (make-listener* listener socket url)))
      (if (= code 0)
          (begin
            ;; no need to close listener on GC
            ;; since it gets closed with its socket
            (set-finalizer! listener free-foreign-nng-listener)
            listener)
          (signal (condition `(exn nng listener message
                                   ,(format "failed to create listener - ~a~n"
                                            (nng-strerror code))))))))

  (define (listener-close! listener)
    (let ((code (listener-close!* listener)))
      (unless (= code 0)
        (signal (condition `(exn nng listener message
                                 ,(format "failed to close listener - ~a~n"
                                          (nng-strerror code))))))))

  (define listener-start!*
    (foreign-lambda* int ((nng-listener listener))
      "C_return(nng_listener_start(*listener, 0));"))

  (define (listener-start! listener)
    (let ((code (listener-start!* listener)))
      (unless (= code 0)
        (signal (condition `(exn nng listener message
                                 ,(format "failed to start listener - ~a~n"
                                          (nng-strerror code))))))))

  (define listener-set-ptr!
    (foreign-lambda* int ((nng-listener listener) (c-string opt) (c-pointer ptr))
      "C_return(nng_listener_set_ptr(*listener, opt, ptr));"))

  ;;;
  ;;; Socket/dialer/listener options
  ;;;
  ;; 4d chess time bois
  (define socket-foreign-getters (make-parameter (make-hash-table)))
  (define socket-foreign-setters (make-parameter (make-hash-table)))

  (define listener-foreign-getters (make-parameter (make-hash-table)))
  (define listener-foreign-setters (make-parameter (make-hash-table)))

  (define dialer-foreign-getters (make-parameter (make-hash-table)))
  (define dialer-foreign-setters (make-parameter (make-hash-table)))

  (define (make-getter getters)
    (lambda (nng-object option)
      (let ((getter (hash-table-ref/default (getters) option #f)))
        (if getter
            (receive (code x) (getter nng-object (option->foreign-name option))
              (if (= code 0)
                  x
                  (signal (condition `(exn nng option message ,(nng-strerror code))))))
            (signal (condition `(exn nng option message
                                     ,(format "option not supported: ~a~n" option))))))))

  (define (make-setter setters)
    (lambda (nng-object option x)
      (let ((setter (hash-table-ref/default (setters) option #f)))
        (if setter
            (let ((code (setter nng-object (option->foreign-name option) x)))
              (unless (= code 0)
                (signal (condition `(exn nng option message ,(nng-strerror code))))))
            (signal (condition `(exn nng option message
                                     ,(format "option not supported: ~a~n" option))))))))

  (define socket-get (make-getter socket-foreign-getters))
  (define socket-set! (make-setter socket-foreign-setters))

  (define listener-get (make-getter listener-foreign-getters))
  (define listener-set! (make-setter listener-foreign-setters))

  (define dialer-get (make-getter dialer-foreign-getters))
  (define dialer-set! (make-setter dialer-foreign-setters))

  (define (option->foreign-name option)
    (case option
      ((nng/recvtimeo) (foreign-value NNG_OPT_RECVTIMEO c-string))
      ((nng/sendtimeo) (foreign-value NNG_OPT_SENDTIMEO c-string))
      ((nng/sub-subscribe) (foreign-value NNG_OPT_SUB_SUBSCRIBE c-string))
      ((nng/recvmaxsz) (foreign-value NNG_OPT_RECVMAXSZ c-string))
      (else (signal (condition `(exn nng option message
                                     ,(format "option not supported: ~a~n" option)))))))

  ;;;
  ;;; usage: (define-foreign-accessor/mutator!
  ;;;          <name> <getter/setter table>
  ;;;          (<option> . <options>)
  ;;;          <foreign-lambda* or foreign-primitive>)
  ;;;
  ;;; options should be present in option->foreign-name
  ;;;
  ;;; getters should use a foreign-primitive and return two values:
  ;;; a return code and the value itself (or C_SCHEME_FALSE if there is an error)
  ;;;
  (define-syntax define-foreign-accessor/mutator!
    (syntax-rules ()
      ((_ name table (opt . opts) accessor/mutator)
       (begin
         (define name accessor/mutator)
         (for-each (lambda (option)
                     (hash-table-set! (table) option name))
                   '(opt . opts))))))

  (define-foreign-accessor/mutator! nng-socket-set-size!
    socket-foreign-setters (nng/recvmaxzs)
    (foreign-lambda* int ((nng-socket socket) (c-string option) (size_t sz))
      "C_return(nng_socket_set_size(*socket, option, sz));"))

  (define-foreign-accessor/mutator! nng-socket-set-ms!
    socket-foreign-setters (nng/recvtimeo nng/sendtimeo)
    (foreign-lambda* int ((nng-socket socket) (c-string option) (int32 ms))
      "C_return(nng_socket_set_ms(*socket, option, ms));"))

  (define-foreign-accessor/mutator! nng-socket-set-string-minus-null!
    socket-foreign-setters (nng/sub-subscribe)
    (foreign-lambda* int ((nng-socket socket) (c-string option) (c-string val))
      "C_return(nng_socket_set(*socket, option, (void*) val, strlen(val)));"))

  ;;;
  ;;; TLS configuration
  ;;;
  (define tls-register!*
    (foreign-lambda int "nng_tls_register"))

  (define (tls-register!)
    (let ((code (tls-register!*)))
      (unless (= code 0)
        (signal (condition `(exn nng tls message
                                 ,(format "failed to register tls transport - ~a~n"
                                          (nng-strerror code))))))))

  (define tls-engine-name
    (foreign-lambda c-string "nng_tls_engine_name"))

  (define (tls-enabled?)
    (let ((engine-name (tls-engine-name)))
      (if (string=? engine-name "none")
          #f
          #t)))

  (define set-listener-tls-own-certificate!*
    (foreign-lambda* int ((nng-listener listener)
                          (c-string cert)
                          (c-string key)
                          (c-string pass)
                          (bool enforce_client_auth))
      "nng_tls_config* config;
      int code = nng_tls_config_alloc(&config, NNG_TLS_MODE_SERVER);
      if (code != 0) {
	      return (code);
	  }

      code = nng_tls_config_own_cert(config, cert, key, pass);
      if (code != 0) {
          goto out;
	  }

      nng_tls_auth_mode mode = enforce_client_auth ? NNG_TLS_AUTH_MODE_REQUIRED : NNG_TLS_AUTH_MODE_NONE;
      code = nng_tls_config_auth_mode(config, mode);
      if (code != 0) {
          goto out;
      }

      code = nng_listener_setopt_ptr(*listener, NNG_OPT_TLS_CONFIG, config);

    out:
      nng_tls_config_free(config);
      return (code);"))

  (define (set-listener-tls-own-certificate! listener cert key enforce-client-auth? #!optional (pass #f))
    (let ((code (set-listener-tls-own-certificate!* listener cert key pass enforce-client-auth?)))
      (unless (= code 0)
        (signal (condition `(exn nng tls message
                                 ,(format "failed to set listener's own certificate - ~a~n"
                                          (nng-strerror code))))))))

  (define set-dialer-tls-certificate!*
    (foreign-lambda* int ((nng-dialer dialer)
                          (c-string cert)
                          (c-string key)
                          (c-string pass)
                          (c-string server_name))
      "nng_tls_config *config;
	  int code = nng_tls_config_alloc(&config, NNG_TLS_MODE_CLIENT);
	  if (code != 0) {
	      return (code);
	  }

      code = nng_tls_config_ca_chain(config, cert, NULL);
      if (code != 0) {
	      goto out;
	  }

      code = nng_tls_config_server_name(config, server_name);
	  if (code != 0) {
	      goto out;
	  }

      code = nng_tls_config_auth_mode(config, NNG_TLS_AUTH_MODE_REQUIRED);
      if (code != 0) {
          goto out;
      }

      if (key != NULL) {
          code = nng_tls_config_own_cert(config, cert, key, pass);
          if (code != 0) {
              goto out;
          }
      }

      code = nng_dialer_set_ptr(*dialer, NNG_OPT_TLS_CONFIG, config);

    out:
	  nng_tls_config_free(config);
	  return (code);"))

  (define (set-dialer-tls-certificate! dialer chain #!optional (server-name "localhost") (key #f) (pass #f))
    (let ((code (set-dialer-tls-certificate!* dialer chain key pass server-name)))
      (unless (= code 0)
        (signal (condition `(exn nng tls message
                                 ,(format "failed to set client certificate - ~a~n"
                                          (nng-strerror code))))))))

  ;;;
  ;;; Socket creation/destruction
  ;;;
  (define (type->socket-creator type)
    (case type
      ((req) nng-req-open)
      ((rep) nng-rep-open)
      ((push) nng-push-open)
      ((pull) nng-pull-open)
      ((pub) nng-pub-open)
      ((sub) nng-sub-open)
      ((pair) nng-pair-open)
      ((surveyor) nng-surveyor-open)
      ((respondent) nng-respondent-open)
      ((bus) nng-bus-open)
      (else (signal (condition '(exn nng socket message "invalid socket type"))))))

  (define (make-socket type)
    (let* ((socket (make-foreign-nng-socket))
           (code ((type->socket-creator type) socket)))
      (if (= code 0)
          (begin
            (set-finalizer! socket
                            (lambda (s)
                              (nng-close! s)
                              (free-foreign-nng-socket s)))
            socket)
          (signal (condition `(exn nng socket message
                                   ,(format "failed to create ~a socket - ~a~n"
                                            (symbol->string type)
                                            (nng-strerror code))))))))

  (define (make-req-socket)
    (make-socket 'req))

  (define (make-rep-socket)
    (make-socket 'rep))

  (define (make-push-socket)
    (make-socket 'push))

  (define (make-pull-socket)
    (make-socket 'pull))

  (define (make-pub-socket)
    (make-socket 'pub))

  (define (make-sub-socket)
    (make-socket 'sub))

  (define (make-pair-socket)
    (make-socket 'pair))

  (define (make-surveyor-socket)
    (make-socket 'surveyor))

  (define (make-respondent-socket)
    (make-socket 'respondent))

  (define (make-bus-socket)
    (make-socket 'bus))

  (define nng-close!*
    (foreign-lambda* int ((nng-socket socket))
      "C_return(nng_close(*socket));"))

  (define (nng-close! socket)
    (let ((code (nng-close!* socket)))
      (if (or (= code 0) (= code (foreign-value NNG_ECLOSED int)))
          (begin) ; TODO: should free socket here?
          (signal (condition `(exn nng socket message
                                   ,(format "failed to close socket - ~a~n"
                                            (nng-strerror code))))))))

  (define (socket-finalize! socket)
    (nng-close!* socket)
    (free-foreign-nng-socket socket))

  ;;;
  ;;; Socket creation, send/recv
  ;;;
  (define nng-dial*
    (foreign-lambda* int ((nng-socket socket) (c-string addr) (nng-dialer dialer))
      "C_return(nng_dial(*socket, addr, dialer, 0));"))

  (define (nng-dial socket address #!optional (dialer #f))
    (let ((code (nng-dial* socket address dialer)))
      (unless (= code 0)
        (signal (condition `(exn nng socket message
                                 ,(format "failed to dial ~a - ~a~n"
                                          address
                                          (nng-strerror code))))))))

  (define nng-listen*
    (foreign-lambda* int ((nng-socket socket) (c-string addr) (nng-listener listener))
      "C_return(nng_listen(*socket, addr, listener, 0));"))

  (define (nng-listen socket address #!optional (listener #f))
    (let ((code (nng-listen* socket address listener)))
      (unless (= code 0)
        (signal (condition `(exn nng socket message
                                 ,(format "failed to listen on socket at ~a - ~a~n"
                                          address
                                          (nng-strerror code))))))))

  (define nng-send*
    (foreign-lambda* int ((nng-socket socket) (c-string msg) (size_t size))
      "C_return(nng_send(*socket, (void*) msg, size + 1, 0));"))

  (define (nng-send socket msg)
    (let ((code (nng-send* socket msg (string-length msg))))
      (unless (= code 0)
        (signal (condition `(exn nng send message
                                 ,(format "failed to send message - ~a~n"
                                          (nng-strerror code))))))))

  (define socket-fd
    (foreign-lambda* int ((nng-socket socket))
      "int fd;"
      "nng_socket_get_int(*socket, NNG_OPT_RECVFD, &fd);"
      "C_return(fd);"))

  (define nng-recv*
    (foreign-primitive ((nng-socket socket) (bool non_blocking))
        "/* comment here because I wanted the lines below to align and geiser wasn't having any of that otherwise */"
      "char *buffer = NULL;"
      "size_t size;"
      "int flags = non_blocking ? NNG_FLAG_ALLOC | NNG_FLAG_NONBLOCK : NNG_FLAG_ALLOC;"
      "int ret_code = nng_recv(*socket, &buffer, &size, flags);"

      "if (ret_code != 0) {"
      "    C_word vals[4] = { C_SCHEME_UNDEFINED, C_k, C_fix(ret_code), C_SCHEME_FALSE };"
      "    C_values(4, vals);"
      "}"

      "C_word* ret_buf = C_alloc(size);"
      "C_word ret_str = C_string2(&ret_buf, buffer);"

      "nng_free(buffer, size);"

      "C_word vals[4] = { C_SCHEME_UNDEFINED, C_k, C_fix(ret_code), ret_str };"
      "C_values(4, vals);"))

  (define (nng-recv socket #!optional (non-blocking? #f))
    (receive (code msg) (begin
                          ;; TODO: find a way to block only the current thread without using NNG_OPT_RECVFD
                          (unless non-blocking? (thread-wait-for-i/o! (socket-fd socket)))
                          (nng-recv* socket non-blocking?))
      (cond ((and (= code 0) msg) msg)
            ((and non-blocking? (= code (foreign-value "NNG_EAGAIN" int))) #f)
            ((and (not non-blocking?) (= code (foreign-value "NNG_ETIMEDOUT" int))) #f)
            (else (signal (condition `(exn nng recv message
                                           ,(format "failed to receive message - ~a~n"
                                                    (nng-strerror code))))))))))
