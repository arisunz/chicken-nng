(import (chicken format)
        (chicken io)
        (chicken string)
        (chicken tcp)
        nng
        srfi-18
        test
        uuid)

;;;;
;;;; !! DISCLAIMER !!
;;;; I'm no scalability protocols expert. These tests are the BARE MINIMUM to
;;;; ensure that messages flow from one socket to another for the most common
;;;; combinations. I also only tested tcp and inproc transports because that's
;;;; how lazy I am.
;;;;
;;;; If you wanna add more comprehensive tests here go right ahead.
;;;; Issues explaining what should be tested (and why) are also welcome!
;;;;

(define (unused-port)
  (let* ((listener (tcp-listen 0))
         (port (tcp-listener-port listener)))
    (tcp-close listener)
    port))

(define (make-inet-address #!optional (proto "tcp"))
  (let ((port (unused-port)))
    (format "~a://localhost:~a"
            proto
            port)))

(define (make-tcp-address)
  (make-inet-address))

(define (make-tls-address)
  (make-inet-address "tls+tcp"))

(define (make-inproc-address)
  (format "inproc://~a"
          (uuid)))

(tls-register!)

(define cert
  (if (tls-enabled?)
      (with-input-from-file "cert.pem"
        (lambda ()
          (read-string)))
      #f))

(define key
  (if (tls-enabled?)
      (with-input-from-file "key.pem"
        (lambda ()
          (read-string)))
      #f))

;;;
;;; Req-Rep
;;;
(define (make-listening-reply-socket address)
  (let ((socket (make-rep-socket)))
    (nng-listen socket address)
    socket))

(define (make-dialed-request-socket address)
  (let ((socket (make-req-socket)))
    (nng-dial socket address)
    socket))

(define (req-rep-test address)
  (let ((rep (make-listening-reply-socket address))
        (req (make-dialed-request-socket address)))
    (nng-send req "message")
    (nng-recv rep)
    (nng-send rep "message")
    (nng-recv req)))

;;;
;;; Pull-Push
;;;
(define (make-listening-pull-socket address)
  (let ((socket (make-pull-socket)))
    (nng-listen socket address)
    socket))

(define (make-dialed-push-socket address)
  (let ((socket (make-push-socket)))
    (nng-dial socket address)
    socket))

(define (push-pull-test address)
  (let ((pull (make-listening-pull-socket address))
        (push (make-dialed-push-socket address)))
    (nng-send push "message")
    (nng-recv pull)))

;;;
;;; Pair
;;;
(define (make-listening-pair-socket address)
  (let ((socket (make-pair-socket)))
    (nng-listen socket address)
    socket))

(define (make-dialed-pair-socket address)
  (let ((socket (make-pair-socket)))
    (nng-dial socket address)
    socket))

(define (pair-test address)
  (let ((p1 (make-listening-pair-socket address))
        (p2 (make-dialed-pair-socket address)))
    (nng-send p2 "message")
    (nng-recv p1)))

;;;
;;; Pub-Sub
;;;
(define (make-listening-pub-socket address)
  (let ((socket (make-pub-socket)))
    (nng-listen socket address)
    socket))

(define (make-dialed-sub-socket address topic)
  (let ((socket (make-sub-socket)))
    (nng-dial socket address)
    (socket-set! socket 'nng/sub-subscribe topic)
    socket))

(define (pub-sub-test address inproc)
  ;; we REALLY want to make sure everyone is ready in pub-sub
  ;; because if we send messages too quickly they sometimes get lost
  ;; seems like the topic isn't set fast enough, so pub will hang forever when recv'ing
  (let ((coord1 (make-listening-pair-socket inproc))
        (pub (make-listening-pub-socket address))
        (sub-thread (thread-start!
                     (lambda ()
                       (let ((sub (make-dialed-sub-socket address "#topic"))
                             (coord2 (make-dialed-pair-socket inproc)))
                         (nng-send coord2 "boop")
                         (nng-send coord2 (nng-recv sub)))))))
    (nng-recv coord1)
    (nng-send pub "#topic-message")
    (nng-recv coord1)))

;;;
;;; Survey-Respondent
;;;
(define (make-listening-surveyor-socket address)
  (let ((socket (make-surveyor-socket)))
    (nng-listen socket address)
    socket))

(define (make-dialed-respondent-socket address)
  (let ((socket (make-respondent-socket)))
    (nng-dial socket address)
    socket))

(define (surv-resp-test address)
  (let* ((heartbeat-address (make-inproc-address))
         (listener (make-listening-pair-socket heartbeat-address))
         (surv (make-listening-surveyor-socket address))
         (resp-thread (thread-start!
                       (lambda ()
                         (let ((heartbeat (make-dialed-pair-socket heartbeat-address))
                               (resp (make-dialed-respondent-socket address)))
                           (nng-send heartbeat "ping")
                           (nng-recv resp)
                           (nng-send resp "survey response"))))))
    (nng-recv listener)
    ;; TODO: properly synchronize this. blegh.
    (sleep 1)
    (nng-send surv "survey-start")
    (nng-recv surv)))

;;;
;;; Bus
;;;
(define (make-listening-bus-socket address)
  (let ((socket (make-bus-socket)))
    (nng-listen socket address)
    socket))

(define (bus-test address1 address2)
  (let ((p1 (make-listening-bus-socket address1))
        (p2 (make-listening-bus-socket address2)))
    (nng-dial p1 address2)
    (nng-dial p2 address1)
    (nng-send p1 "message")
    (nng-recv p2)))

;;;
;;; TLS
;;;
(define (tls-test address)
  (let* ((pair1 (make-pair-socket))
         (pair2 (make-pair-socket))
         (listener (make-listener pair1 address))
         (dialer (make-dialer pair2 address)))
    (set-listener-tls-own-certificate! listener cert key #f)
    (set-dialer-tls-certificate! dialer cert "127.0.0.1")
    (listener-start! listener)
    (dialer-start! dialer)
    (nng-send pair2 "message")
    (nng-recv pair1)))

(test-group "nng"

            (test "non-blocking"
                  #f
                  (let ((socket (make-listening-pair-socket "inproc://uwu")))
                    (nng-recv socket #t)))

            (test "tcp req-rep"
                  "message"
                  (req-rep-test (make-tcp-address)))

            (test "inproc req-rep"
                  "message"
                  (req-rep-test (make-inproc-address)))

            (test "tcp push-pull"
                  "message"
                  (push-pull-test (make-tcp-address)))

            (test "inproc push-pull"
                  "message"
                  (push-pull-test (make-inproc-address)))

            (test "tcp pair"
                  "message"
                  (pair-test (make-tcp-address)))

            (test "inproc pair"
                  "message"
                  (pair-test (make-inproc-address)))

            (test "tcp pub-sub"
                  "message"
                  (cadr (string-split (pub-sub-test (make-tcp-address) (make-inproc-address)) "-")))

            (test "inproc pub-sub"
                  "message"
                  (cadr (string-split (pub-sub-test (make-inproc-address) (make-inproc-address)) "-")))

            (test "tcp surv-resp"
                  "survey response"
                  (surv-resp-test (make-tcp-address)))

            (test "inproc surv-resp"
                  "survey response"
                  (surv-resp-test (make-inproc-address)))

            (test "tcp bus"
                  "message"
                  (bus-test (make-tcp-address) (make-tcp-address)))

            (test "inproc bus"
                  "message"
                  (bus-test (make-inproc-address) (make-inproc-address)))

            (when (tls-enabled?)
              (test "tls transport"
                    "message"
                    (tls-test (make-tls-address)))))

(test-exit)
