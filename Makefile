SRC=src/*.scm

build: $(SRC)
	chicken-install -n

test: build credentials
	csi tests/run.scm

credentials:
	cp tests/*.pem .

install:
	chicken-install

clean-test: clean build test

clean-build: clean build

clean:
	chicken-clean
