# chicken-nng

Barebones `libnng` bindings for CHICKEN Scheme. rep, req, pull, push, sub, pub, pair, surveyor, respondent and bus sockets are supported. Feedback and patches are very welcome!

# Requirements

* `libnng` (make sure you have a build with shared libs)
* `libmbedtls` (and a TLS-enabled `libnng`) if you plan on using TLS transport.
* CHICKEN 5 with the following eggs:
  * srfi-18
  * foreigners
  * test and miscmacros (for tests)

# Installing

`chicken-install -s` on the project root. The `Makefile` provided is more of a convenience for working on the egg itself and not really intended for anything else.

# Usage

## Socket creation

`[procedure] (make-rep-socket)`

`[procedure] (make-req-socket)`

`[procedure] (make-pull-socket)`

`[procedure] (make-push-socket)`

`[procedure] (make-pub-socket)`

`[procedure] (make-sub-socket)`

`[procedure] (make-pair-socket)`

`[procedure] (make-surveyor-socket)`

`[procedure] (make-respondent-socket)`

`[procedure] (make-bus-socket)`

Spawn a specific socket type. All sockets are closed automatically when their finalizers run.

## Socket/listener/dialer options

`[procedure] (socket-get socket option)`

`[procedure] (socket-set! socket option value)`

`[procedure] (listener-get listener option)`

`[procedure] (listener-set! listener option value)`

`[procedure] (dialer-get dialer option)`

`[procedure] (dialer-set! dialer option value)`

What `option` is object-specific. Currently supported options are:

* `nng/recvtimeo`
* `nng/sendtimeo`
* `nng/sub-subscribe`
* `nng/recvmaxsz`

## Listening/Dialing

`[procedure] (make-listener socket address)`

Make listener from `socket` to bind to `address`.

`[procedure] (make-dialer socket address)`

Make a dialer from `socket` to connect to `address`.

`[procedure] (listener-start! listener)`

Bind listener.

`[procedure] (dialer-start! dialer)`

Connect dialer.

`[procedure] (nng-listen socket address [listener])`

Bind a socket to an address.

`[procedure] (nng-dial socket address [dialer])`

Connect a socket to an address.

## Send/Recv

`[procedure] (nng-send socket msg)`

Send string `msg` through a socket.

`[procedure] (nng-recv socket [non-blocking?])`

Receive msg through a socket. If `non-blocking?` is passed and is not false, `nng-recv` returns `#f` when it would otherwise block. Additionally, if a timeout has been set with `nng-socket-set-recv-timeout!`, `#f` is returned when said timeout is reached.

## Closing

`[procedure] (nng-close! socket)`

Close socket explicitly.

`[procedure] (listener-close! listener)`

Close listener explicitly. Should not be necessary since listeners get close whenever their associated sockets get closed.

`[procedure] (dialer-close! dialer)`

Same as above; usually unnecessary.

## TLS Transport

`[procedure] (tls-register!)`

Register TLS transport. Usually not necessary, but depends on how your `libnng` was built.

`[procedure] (tls-enabled?)`

Check if TLS is enabled in your system.

`[procedure] (set-listener-tls-own-certificate! listener cert key enforce-client-auth? [pass])`

Configure listener with a certificate, secret key and a (possible) passphrase. If `enfonce-client-auth?` is `#t`, it will require clients to authenticate themselves.

`[procedure] (set-dialer-tls-certificate! dialer chain [server-name [key [pass]]])`

Configure dialer with a certificate. `server-name` defaults to localhost. If the certificate is its own, also provide a private key, and passphrase if applicable.

# Bugs/Caveats/TODOs

* Some options don't seem to work properly yet. `nng/recvtimeo` isn't particularly useful since we block the current thread with `thread-wait-for-i/o!` when receiving.
* We use `NNG_OPT_RECVFD` in combination with `thread-wait-for-i/o!` to avoid blocking all threads. Unfortunately, this means we can't use `nng_ctx`.
